#! python
from math import sqrt


class Complex:
    def __init__(self, real = 0, imaginary = 0):
        self.r = real
        self.i = imaginary

    def __add__(self, other):
       return Complex(self.r + other.r, self.i + other.i)

    def __sub__(self, other):
        return Complex(self.r - other.r, self.i - other.i)

    def __abs__(self):
        return sqrt(self.r ** 2 + self.i ** 2)

    def __str__(self):
        return '(%g, %g)' % (self.r, self.i)


# test
c1 = Complex(1, 2)
c2 = Complex(4, -5)

print((c1 + c2))
print((c1 - c2))
print(abs(c1))
