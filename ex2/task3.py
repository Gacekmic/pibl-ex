#! python
from xml.dom.minidom import parse
import xml.sax
import xml.dom.minidom


# DOM

DOMTree = xml.dom.minidom.parse("XMLtest.xml")
collection = DOMTree.documentElement
if collection.hasAttribute("shelf"):
    print("Root element : %s" % collection.getAttribute("shelf"))

planes = collection.getElementsByTagName("plane")

# printing data for each plane
for plane in planes:
    print("____Plane____")
    model = plane.getElementsByTagName('model')[0]
    print("Model: %s" % model.childNodes[0].data)
    year = plane.getElementsByTagName('year')[0]
    print("Production date: %s" % year.childNodes[0].data)
    factory = plane.getElementsByTagName('make')[0]
    print("Company: %s" % factory.childNodes[0].data)
    col = plane.getElementsByTagName('color')[0]
    print("Color: %s" % col.childNodes[0].data)
# -------------------------------------------------------------------------------------------
# SAX


class PlanesHandler(xml.sax.ContentHandler):
    def __init__(self):
        super(PlanesHandler, self).__init__()
        self.CurrentData = ""
        self.make = ""
        self.model = ""
        self.year = ""
        self.color = ""

    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "plane":
            print("____Plane____")
            mod = attributes["model"]
            print("Plane: ", mod)

    def endElement(self, tag):
        if self.CurrentData == "make":
            print("Factory:", self.make)
        elif self.CurrentData == "year":
            print("Production date:", self.year)
        elif self.CurrentData == "color":
            print("Color:", self.color)
        self.CurrentData = ""

    def characters(self, content):
        if self.CurrentData == "make":
            self.make = content
        elif self.CurrentData == "model":
            self.model = content
        elif self.CurrentData == "year":
            self.year = content
        elif self.CurrentData == "color":
            self.color = content


parser = xml.sax.make_parser()
parser.setFeature(xml.sax.handler.feature_namespaces, 0)
Handler = PlanesHandler()
parser.setContentHandler(Handler)
parser.parse("XMLtest.xml")
