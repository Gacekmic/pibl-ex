#! phyton
import math

a, b, c = input("Enter a, b, c: ").split()
a = int(a)
b = int(b)
c = int(c)

d = b**2 - 4*a*c
print("delta = " + str(d))
if d > 0:
    print("x1 = " + str((-b-math.sqrt(d))/(2*a)))
    print("x2 = " + str((-b+math.sqrt(d))/(2*a)))
elif d == 0:
    print("x0 = " + str((-b/(2*a))))
else:
    print("No real roots!")
