#! python
import numpy

size = 8

a = numpy.random.randint(0, 100, (size, size))
b = numpy.random.randint(0, 100, (size, size))

out = numpy.matmul(a, b)
print(out)
