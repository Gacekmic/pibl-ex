#! python

file_in = open("ex1/text.txt", "r")
file_out = open("ex1/rep_text.txt", "w+")
words = {
    'i': 'oraz',
    'oraz': 'i',
    'nigdy': 'prawie nigdy',
    'dlaczego': 'czemu'
}

for line in file_in:
    for word in words:
        line = line.replace(word, words[word])
    file_out.write(line)
file_in.close()
file_out.close()
