#! python

file_in = open("ex1/text.txt", "r")
file_out = open("ex1/del_text.txt", "w+")
del_list = ['sie', 'i ', 'oraz', 'nigdy', 'dlaczego']
for line in file_in:
    for word in del_list:
        line = line.replace(word, "")
    file_out.write(line)
file_in.close()
file_out.close()
