#! python
from random import randint
import copy

values =[]
mySorted =[]
defSorted =[]

for i in range(50):
    tmp = randint(0, 100)
    values.append(tmp)

mySorted = copy.deepcopy(values)
defSorted = copy.deepcopy(values)   # for testing purposes

for i in range(len(mySorted),0,-1):
    for idx in range(i-1):
        if mySorted[idx] < mySorted[idx+1]:
            tmp = mySorted[idx]
            mySorted[idx] = mySorted[idx + 1]
            mySorted[idx+1] = tmp

defSorted.sort(reverse=True)

print(values)
print(mySorted)
print(defSorted)
