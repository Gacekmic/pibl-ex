#! python
import os

# function will change all files *.jpg in current directory
# into files *.png
file_names = os.listdir('.')
for file in file_names:
    if '.jpg' in file:
        name = file.split('.')[0]
        new_file_name = name + '.png'
        os.rename(file, new_file_name)
