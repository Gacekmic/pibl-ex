#! python

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

if len(a) == len(b):
    dot = 0
    for i in range(len(a)):
        dot += (a[i]*b[i])
    print("Dot product of vectors a and b: " + str(dot))
else:
    print("Vectors have different length!")

