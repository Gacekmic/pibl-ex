#! python
import numpy

size = 128

a = numpy.random.randint(0, 100, (size, size))
b = numpy.random.randint(0, 100, (size, size))

out = numpy.matrix.sum(a, b)
print(out)
